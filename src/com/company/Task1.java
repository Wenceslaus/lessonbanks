package com.company;

import com.company.data.Generator;
import com.company.model.BanksHolder;

import java.util.Scanner;

public class Task1 {

    public void run() {
        BanksHolder banksHolder = new BanksHolder(Generator.generate());
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите количесвто денег, которое хотите обменять: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Введите банк через который хотите совершить обмен (ПриватБанк, ОщадБанк или ПУМБ): ");
        String bank = scan.nextLine();

        System.out.println("Введите валюту (USD, EUR или RUB): ");
        String currency = scan.nextLine();

        banksHolder.calc(bank, currency, amount);
    }
}
