package com.company.model;

public class BanksHolder {

    private Bank[] banks;

    public BanksHolder(Bank[] banks) {
        this.banks = banks;
    }

    public void calc(String bankName, String currency, int amount) {
        for (Bank currentBank : banks) {
            if (currentBank.isSameAs(bankName)) {
                currentBank.convert(currency, amount);
            }
        }
    }
}
