package com.company.model;

import java.util.Locale;

public class Bank {
    private String name;
    private Currency[] rates;

    public Bank(String name, Currency[] rates) {
        this.name = name;
        this.rates = rates;
    }

    public boolean isSameAs(String bankName) {
        return name.equalsIgnoreCase(bankName);
    }

    public void convert(String currency, int amount) {
        for (Currency currentRate : rates) {
            if (currentRate.getName().equalsIgnoreCase(currency)) {
                System.out.println(String.format(Locale.US, "Ваши деньги в %s: %.2f", currency, amount / currentRate.getRate()));
            }
        }
    }
}
