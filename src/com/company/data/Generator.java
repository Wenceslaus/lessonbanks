package com.company.data;

import com.company.model.Bank;
import com.company.model.Currency;

public final class Generator {

    public static final int BANKS_NUMBER = 2;

    private Generator() {
    }

    public static Bank[] generate() {
        Bank[] banks = new Bank[BANKS_NUMBER];

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 28.5f);
            currencies[1] = new Currency("eur", 31.5f);
            currencies[2] = new Currency("gbp", 70.5f);
            banks[0] = new Bank("ПриватБанк", currencies);
        }

        {
            Currency[] currencies = new Currency[3];
            currencies[0] = new Currency("usd", 28.5f);
            currencies[1] = new Currency("eur", 31.5f);
            currencies[2] = new Currency("gbp", 70.5f);
            banks[1] = new Bank("MonoBank", currencies);
        }

        return banks;
    }
}
